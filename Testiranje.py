import numpy
import logging
import sys

logger = logging.getLogger('program_convolution')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('spam.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

def normaliziraj(vektor):
    logger.info("Normalizing...")
    vec = numpy.array(vektor[0], dtype=float)
    maxNum = vec[0]
    
    for x in vec:
        if x < 0:
            x = x*(-1)
        if x > maxNum:
            maxNum = x
    for x in range(len(vec)):
        vec[x] = (vec[x]/maxNum)
    return vec

def konv_cas_mono(signal, impulz):
    logger.info("Calling funciton konv_cas_mono")
    try:
        result = signal*impulz.T
    except Exception as e:
        logger.error("Invalid shapes of matrix")
        return numpy.zeros(0,0)
    else:
        logger.info("Matrix shapes - OK")
        print(result)     
        impulz = impulz.T
        A = numpy.zeros((numpy.size(signal),numpy.size(signal)+numpy.size(impulz)-1))
        i = 0
        j = 0
        C = numpy.zeros((1,numpy.size(signal)+numpy.size(impulz)-1))
        B = signal[0::1]*impulz[0::1]
        copy_matrixB = numpy.array(B)
        for x in range(signal.shape[0]-1):
            copy_matrixB = numpy.insert(copy_matrixB,impulz.shape[1],0,axis=1)
        for x in range(signal.shape[0]):
            if x==0:
                copy_matrixB[0::signal.shape[0]]==copy_matrixB[0::signal.shape[0]]
            else:
                copy_matrixB[x::signal.shape[0]] = numpy.pad(copy_matrixB[x::signal.shape[0]],((0,0),(x,0)), mode='constant')[:,:-x]
        B = numpy.zeros((1,numpy.size(signal)+numpy.size(impulz)-1))
        print(B[0])
        for i in range(numpy.size(signal)+numpy.size(impulz)-1):
            try : 
               B[0][i] = sum(copy_matrixB[:,i])
            except Exception :
               logger.error("Index out of bounds")
               return numpy.zeros(1)
        B = normaliziraj(B)
        B.shape = (1,numpy.size(signal)+numpy.size(impulz)-1)
        logger.info("Finalizing function konv_cas_mono")
        return B.T

def konv_cas_stereo(signal, impulz):
    impulz = impulz.T
    A = numpy.zeros((signal.shape[0],signal.shape[0]+impulz.shape[0]-1))
    C = numpy.zeros((1,signal.shape[0]+impulz.shape[0]-1))
    
    B = B = signal[0::1,0]*impulz[0,0::1]
    copy_matrixB = numpy.array(B)
    for x in range(signal.shape[0]-1):
        copy_matrixB = numpy.insert(copy_matrixB,impulz.shape[1],0,axis=1)
    for x in range(signal.shape[0]):
        if x==0:
            copy_matrixB[0::signal.shape[0]]==copy_matrixB[0::signal.shape[0]]
        else:
            copy_matrixB[x::signal.shape[0]] = numpy.pad(copy_matrixB[x::signal.shape[0]],((0,0),(x,0)), mode='constant')[:,:-x]
    B = numpy.zeros((1,signal.shape[0]+impulz.shape[0]))
    
    for i in range(signal.shape[0]+impulz.shape[0]):
        B[0][i] = sum(copy_matrixB[:,i])
    B = normaliziraj(B)

    B.shape = (1,signal.shape[0]+impulz.shape[0])
    
    d = numpy.array(impulz[1])
    e = numpy.array(signal[0::1,1])
    D = numpy.zeros((signal.shape[0],signal.shape[0]+impulz.shape[0]-1))
    E = numpy.zeros((1,signal.shape[0]+impulz.shape[0]-1))
    F = signal[0::1,1]*impulz[1,0::1]
    copy_matrixF = numpy.array(F)
    for x in range(signal.shape[0]-1):
        copy_matrixF = numpy.insert(copy_matrixF,impulz.shape[1],0,axis=1)
    for x in range(signal.shape[0]):
        if x==0:
            copy_matrixF[0::signal.shape[0]]==copy_matrixF[0::signal.shape[0]]
        else:
            copy_matrixF[x::signal.shape[0]] = numpy.pad(copy_matrixF[x::signal.shape[0]],((0,0),(x,0)), mode='constant')[:,:-x]
    F = numpy.zeros((1,signal.shape[0]+impulz.shape[0]))
    for i in range(signal.shape[0]+impulz.shape[0]):
        F[0][i] = sum(copy_matrixF[:,i])
    F = normaliziraj(F)
    F.shape = (1,signal.shape[0]+impulz.shape[0])
    
    H = numpy.array((B[0]))
    J = numpy.array((F[0]))
    G = numpy.column_stack((H,J))
    logger.info("Finishing function 'konv_cas_stereo'")
    return G

if __name__ == '__main__':
    if sys.argv[1] == "A" :
        A = numpy.matrix([[4,2],[1,3],[2,4],[2,5],[3,6]])
        B = numpy.matrix([[1,2],[2,3],[2,4]])
    elif sys.argv[1] == "B" :
        A = numpy.matrix([[4],[1],[2],[2],[3]])
        B = numpy.matrix([[1],[2],[2]])
    elif sys.argv[1] == "C" :
        A = numpy.matrix([[2],[3],[4],[5],[6]])
        B = numpy.matrix([[2],[3],[4]])
    elif sys.argv[1] == "D" :
        A = numpy.matrix([[4],[1],[2],[2],[3]])
        B = numpy.matrix([[2],[3],[4]])
    elif sys.argv[1] == "E" :
        A = numpy.matrix([[2],[3],[4]])
        B = numpy.matrix([[2],[3],[4]])
    elif sys.argv[1] == "F" :
        A = numpy.matrix([[4,2],[1,3],[2,4],[2,5],[3,6]])
        B = numpy.matrix([[2],[3],[4]])
    else : 
        logger.error("Wrong argument 1. Closing...")
        logging.error("Wrong argument 1. Closing...")
        sys.exit()
    if sys.argv[2] == '1' :
        result_of_function = konv_cas_mono(A,B)
        if len(result_of_function) <= 1 :
            logger.warning("Function closed with error")
        else : 
            print(result_of_function)
    elif sys.argv[2] == '2' :
        result_of_function = konv_cas_stereo(A,B)
        if len(result_of_function) <= 1 :
            logger.warning("Function closed with error")
        else : 
            print(result_of_function)
    else :
        logger.error("Wrong argument 1. Closing...")
        logging.error("Wrong argument 1. Closing...")
        sys.exit()