FROM ubuntu:20.04

RUN apt-get update && apt-get -y install gcc-avr avr-libc

RUN apt-get -y install python
RUN apt-get -y install pylint
RUN apt-get -y install python-numpy

ADD . ./home

WORKDIR ../home

CMD ["pylint","Testiranje.py"]
